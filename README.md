# Movie Trivia Game

This is the project made as a programming assessment. I've taken some decisions that differ from the original specification of the test, such as technology and the game itself.

## The Game

![Movie Trivia Game](http://i.imgur.com/iInRrrv.png)

More screenshots available here: [Movie Trivia Gallery](http://imgur.com/a/QZEBe).

Try to guess the release year of the Top Rated Movies from IMDb! Challenge your friends to quick matches and show them you know all about movies.

The game is available on these platforms:

 - [Windows](http://labs.crocidb.com/movie-trivia/windows.zip)

## Server

The server is a RESTful api, nodejs based express application. It uses SQLite3 for database simply because it's simple enough for this project.

After struggling a few hours with ASP.NET WebAPI, I realized that doing this with Node would allow me to focus better on other aspects of the game, such as making it a little bit more interesting than the requirement.

### Libraries

 - **express**: the mainstream nodejs web framework
 - **sqlite3**: simple sql-based database management
 - **request**: simple library to help making HTTP requests
 - **cheerio**: jquery-like DOM selectors for backend javascript, to help scraping the IMDb page

### Quick RESTful API Specification

This service has a very simple API:

| Method Call | Request Headers | Response | Description |
|--------|------|-----------------|----------|-------------|
| GET <br>/users | **name**: the name of the user | `{ "id": 1,\ "name": "bruno", "played": 11, "won": 7 }` | Returns all the info of a player specicified by its name. If it doesn't exist, it will be created. |
| GET <br>/users/list | **uid**: the id of the current user logged | `[{ "id": 1, "name": "bruno" }, ...]` | Returns all the players registered on the game but the current logged one. |
| POST <br>/game/create | **uid**: the id of the logged user<br>**fid**: friend id to create a game against | `{"id":17, "user_invite":1, "user_accepted":2, ...}` | Returns the game just created |
| GET <br>/game/list | **uid**: the id of the logged user | An array of basic information | Returns the list of games currently active for the given user. |
| GET <br>/game | **uid**: the id of the logged user<br>**gid**: the id of the game about to start | Returns all the data for the game and an array of movies, like that:<br>`{"id":21,"name":"Movie Name","year":1995,"link":"/","cover_link":"/"}` | Fetches all the information of a specific game and all the data from the movies. |
| POST <br>/game | **uid**: the id of the logged user<br>**gid**: the id of the game<br>**answers**: a json of pairs (id, years) | A game object with updated info. | Updates a game with this player answers. If this is the latest player playing the game, it closes the game and distribute the score. |

### How to Run

1. Install NodeJS and NPM tools
2. Inside the project `Server/` directory, run `$ npm install`
3. Make sure you have network connection
4. It probably will start the *express* server running on port *3000*

### Points to Improve

 - **Authentication**: currently, all the authentication implemented in the project is the player ID for each request. 
 - **Validations**: no validations are made with `uid` for most requests, if one user tries to answer the game of other user, that *would be currently allowed*.
 - **Queries**: all the SQL queries are directly into javascript source. Obviously in a "more serious project", we would be using a ORM, but a simple query encapsulation would make a huge improve.
 - **Migrations**: the database is now created using queries form the `defaultTables.sql` file. Any migrations system would help to create a more confortable database environment.

## Client

The client component of this game was created in Unity3D using C# as the main language. Since there were some inconsistencies on the technology required for this test, Unity was choosen because I have more experience with it.

### How to Run

The Unity project is located under the `Client/` directory. After installing Unity3D 5.3.x+, just open the `Game.unity` scene and hit **run**, or download one of the available builds. When selecting the `GameManager` object in scene, a `Service URL` property will be available. It's set default to the service already running on my server, `http://crocidb.com:3066`.

### Third Party libraries

 - [UnityHTTP](https://github.com/andyburke/UnityHTTP): a high-level HTTP/REST client library.

### Points to Improve

 - **Validations**: there's no input validation in the game. You can even login with *""* (empty) user.
 - **Different platforms**: when I started the project I wanted to build it for multiple platforms such as Windows, Android and WebGL. I realized too late that there are some gotchas:
   -  Android: even though the layout works on all the platforms, I didn't realize the fonts aren't working very well on the layout 
   - WebGL: the UnityHTTP library won't build to Javascript
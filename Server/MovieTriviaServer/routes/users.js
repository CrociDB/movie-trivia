var express = require('express');
var router = express.Router();

var UserManager = require('../models/user_manager');
var userManager = new UserManager();

router.get('/', function(req, res, next) {
    if (req.headers.name == undefined) {
        res.status(400).json({});
        return;
    }

    var name = req.headers.name.toLowerCase();
    userManager.getUserByName(name, function(user) {
        res.status(200).json(user);
    });
});

router.get('/list', function(req, res, next) {
    if (req.headers.uid == undefined) {
        res.status(400).json({});
        return;
    }

    userManager.getUserListButMe(req.headers.uid, function(users) {
        res.status(200).json(users);
    });
});

module.exports = router;

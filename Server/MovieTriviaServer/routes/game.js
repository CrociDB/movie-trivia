var express = require('express');
var router = express.Router();

var UserManager = require('../models/user_manager');
var userManager = new UserManager();

var GameManager = require('../models/game_manager');
var gameManager = new GameManager();

router.get('/', function(req, res, next) {
    if (req.headers.uid == undefined || req.headers.gid == undefined) {
        res.status(400).json({});
        return;
    }

    gameManager.getGame(req.headers.gid, function(result) {
        res.status(200).json(result);
    });
});

router.post('/', function(req, res, next) {
    if (req.headers.uid == undefined || req.headers.gid == undefined || req.headers.answers == undefined) {
        res.status(400).json({});
        return;
    }

    gameManager.updateGame(req.headers.gid, JSON.parse(req.headers.answers), function(result) {
        if (result == null) {
            res.status(401).json({});
        }

        res.status(200).json(result);
    });
});

router.get('/list', function(req, res, next) {
    if (req.headers.uid == undefined) {
        res.status(400).json({});
        return;
    }

    gameManager.getAllGames(req.headers.uid, function(result) {
        res.status(200).json(result);
    });
});

router.post('/create', function(req, res, next) {
    if (req.headers.uid == undefined || req.headers.fid == undefined) {
        res.status(400).json({});
        return;
    }

    gameManager.createGame(req.headers.uid, req.headers.fid, function(result) {
        res.status(200).json(result);
    });
});

module.exports = router;

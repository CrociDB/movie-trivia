
var UserManager = function() {

};

UserManager.prototype.getUserByName = function (name, callback) {
    if (!callback) {
        return;
    }

    var movieManager = global.MovieManager;

    movieManager.db.get('SELECT * FROM Users WHERE name = "' + name + '"', function (err, row) {
        if (row) {
            console.log(row);
            callback(row);
            return;
        }

        var stmt = movieManager.db.prepare('INSERT INTO Users(name) VALUES(?)');
        stmt.run(name);
        stmt.finalize();

        movieManager.db.get('SELECT * FROM Users WHERE name = "' + name + '"', function (err, row) {
            if (row) {
                console.log(row);
                callback(row);
                return;
            }
        });
    });
};

UserManager.prototype.getUserListButMe = function (uid, callback) {
    if (!callback) {
        return;
    }
    
    var movieManager = global.MovieManager;

    movieManager.db.all('SELECT * FROM Users WHERE id <> ' + uid, function(err, rows) {
        callback(rows);
    });
};

module.exports = UserManager;

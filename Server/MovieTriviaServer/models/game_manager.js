
var GameManager = function() {

};

GameManager.prototype.createGame = function (uid, fid, callback) {
    if (!callback) {
        return;
    }

    var movieManager = global.MovieManager;

    movieManager.db.all('SELECT id FROM Movies ORDER BY RANDOM() LIMIT 8;', function (err, rows) {
        if (rows) {
            var m = [];
            rows.forEach(function(elem) {
                m.push(elem.id);
            });
            var allMovies = m.join(',');

            var stmt = movieManager.db.prepare('INSERT INTO Games(user_invite, user_accepted, movies) VALUES(?, ?, ?)');
            stmt.run(uid, fid, allMovies, function() {
                movieManager.db.get('SELECT * FROM Games WHERE id = ' + this.lastID, function (err, row) {
                    if (row) {
                        callback(row);
                        return;
                    }
                });
            });
            stmt.finalize();
        }

    });
};

GameManager.prototype.getAllGames = function (uid, callback) {
    if (!callback) {
        return;
    }

    var movieManager = global.MovieManager;

    var sql = "SELECT \
                	Games.id, \
                	Games.user_invite, \
                	Games.user_accepted, \
                	Games.score_user_invited, \
                	Games.score_user_accepted, \
                	uinvite.name as invited_user, \
                	uaccepted.name as accepted_user, \
                    status \
                FROM \
                	Games \
                INNER JOIN Users AS uinvite \
                	ON uinvite.id = Games.user_invite \
                INNER JOIN Users AS uaccepted \
                	ON uaccepted.id = Games.user_accepted \
                WHERE \
                	(Games.user_invite = ? OR Games.user_accepted = ?) AND status < 2";

    movieManager.db.all(sql, {1: uid, 2: uid }, function (err, rows) {
        callback(rows);
    });
};

GameManager.prototype.getGame = function (gid, callback) {
    if (!callback) {
        return;
    }

    var movieManager = global.MovieManager;

    movieManager.db.get("SELECT * FROM Games WHERE id = ?", {1: gid }, function (err, row) {
        if (row) {
            movieManager.db.all("SELECT * FROM Movies WHERE id IN (" + row.movies + ")", function (err, rows) {
                row.movies_detail = rows;
                callback(row);
            });
        }
    });
};

GameManager.prototype.updateGame = function (gid, answers, callback) {
    if (!callback) {
        return;
    }

    var movieManager = global.MovieManager;
    var that = this;

    this.getGame(gid, function(game) {
        /*if (game.status > 1) {
            callback(null);
        }*/

        var score = 0;
        game.movies.split(",").forEach(function(elem) {
            answer = that.getMovieFromListById(answers, parseInt(elem));
            movie = that.getMovieFromListById(game.movies_detail, parseInt(elem));

            if (movie.year == answer.year) {
                score += 5;
            } else {
                score = Math.max(0, score - 3);
            }
        });

        if (game.status == 0) {
            game.score_user_invited = score;
        } else if (game.status == 1) {
            game.score_user_accepted = score;
        }

        game.status++;

        movieManager.db.run("BEGIN TRANSACTION");

        if (game.status == 2) {
            var sql = "UPDATE Users SET played = played + 1, won = won + ? WHERE id = ?";
            var invited = 0;
            var accepted = 0;

            if (game.score_user_invited > game.score_user_invited) {
                invited = 1;
            } else if (game.score_user_accepted > game.score_user_invited) {
                accepted = 1;
            } else {
                invited = accepted = 1;
            }

            movieManager.db.run(sql, { 1: invited, 2: game.user_invite });
            movieManager.db.run(sql, { 1: accepted, 2: game.user_accepted });
        }

        movieManager.db.run("UPDATE Games SET score_user_invited = ?, score_user_accepted = ?, status = ? WHERE id = ?",
                { 1:  game.score_user_invited, 2: game.score_user_accepted, 3: game.status, 4: gid });

        movieManager.db.run("COMMIT");
        game.movies_detail = null;
        callback(game);
    });
};

GameManager.prototype.getMovieFromListById = function (movie_list, id) {
    var movie = null;

    if (movie_list) {
        movie_list.forEach(function (elem) {
            if (elem.id == id) {
                movie = elem;
            }
        });
    }

    return movie;
};

module.exports = GameManager;

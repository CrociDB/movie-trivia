var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');

var sqlite3 = require('sqlite3').verbose();

var db = global.database;

var MovieManager = function() {

};

MovieManager.prototype.initDatabase = function () {
    var db = new sqlite3.Database("./db/database.db");

    var tableData = fs.readFileSync('./sql/defaultTables.sql', 'utf8');
    var that = this;

    db.serialize(function() {
        db.run("BEGIN TRANSACTION");
        var tables = tableData.split("---");
        for (i = 0; i < tables.length; i++)
        {
            db.run(tables[i].trim() + ";");
        }
        db.run("COMMIT");

        db.run("DELETE FROM Movies");
        var stmt = db.prepare("INSERT INTO Movies(id, name, year, link, cover_link) VALUES (?, ?, ?, ?, ?)");

        that.scrapMoviesFromIMdb(function(err, moviesData) {
            if (err) {
                return;
            }

            db.run("BEGIN TRANSACTION");
            var count = 0;
            moviesData.forEach(function(movie) {
                stmt.run(count++, movie.name, movie.year, movie.link, movie.cover);
            });
            db.run("COMMIT");

            stmt.finalize();

            console.log("Server ready!");

            // Uncomment to debug what's been just added
            /*db.each("SELECT * FROM Movies", function(err, row) {
                console.log(row.id + ": " + row.name + " (" + row.year + ")");
            });*/
        });
    });

    this.db = db;
};

MovieManager.prototype.scrapMoviesFromIMdb = function (callback) {
    if (!callback) return;

    var url = 'http://www.imdb.com/chart/top?ref_=nb_mv_3_chttp';
    request(url, function(error, response, html) {
        if (error) {
            callback(error, {});
        }

        var movies = [];

        var $ = cheerio.load(html);
        var ms = $("tbody[class=lister-list] tr");
        ms.each(function (i, elem) {
            var cover = $(this).find("td[class=posterColumn] img").attr("src");
            var name = $(this).find("td[class=titleColumn] a").text();
            var link = $(this).find("td[class=titleColumn] a").attr("href");
            var year = parseInt($(this).find("td[class=titleColumn] span").text().replace('(', '').replace(')', ''));

            movies.push({cover: cover, name: name, link: link, year: year});
        });

        callback(false, movies);
    });
};

module.exports = MovieManager;

﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Communication
{
    public class Communicator
    {
        private static Communicator instance;

        public static Communicator Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Communicator();
                }

                return instance;
            }
        }

        private string m_BaseUrl;

        public string BaseURL
        {
            set
            {
                m_BaseUrl = value;
            }
        }

        public void LoginUser(string userName, Action<Hashtable> callback)
        {
            if (callback == null)
            {
                return;
            }

            Dictionary<string, string> header = new Dictionary<string, string>();
            header.Add("name", userName);

            HTTP.Request req = new HTTP.Request("get", m_BaseUrl + "/users", header);

            req.Send((request) =>
            {
                Debug.Log(request.response.status);

                Hashtable response = request.response.Object;

                callback.Invoke(response);
            });
        }

        public void GetUserList(int uid, Action<ArrayList> callback)
        {
            if (callback == null)
            {
                return;
            }

            Dictionary<string, string> header = new Dictionary<string, string>();
            header.Add("uid", uid.ToString());

            HTTP.Request req = new HTTP.Request("get", m_BaseUrl + "/users/list", header);

            req.Send((request) =>
            {
                Debug.Log(request.response.status);

                bool result = false;
                ArrayList obj = (ArrayList)JSON.JsonDecode( request.response.Text, ref result );

                callback.Invoke(obj);
            });
        }

        public void GetGameList(int uid, Action<ArrayList> callback)
        {
            if (callback == null)
            {
                return;
            }

            Dictionary<string, string> header = new Dictionary<string, string>();
            header.Add("uid", uid.ToString());

            HTTP.Request req = new HTTP.Request("get", m_BaseUrl + "/game/list", header);

            req.Send((request) =>
            {
                Debug.Log(request.response.status);

                bool result = false;
                ArrayList obj = (ArrayList)JSON.JsonDecode(request.response.Text, ref result);

                callback.Invoke(obj);
            });
        }

        public void GetGame(int uid, int gid, Action<Hashtable> callback)
        {
            if (callback == null)
            {
                return;
            }

            Dictionary<string, string> header = new Dictionary<string, string>();
            header.Add("uid", uid.ToString());
            header.Add("gid", gid.ToString());

            HTTP.Request req = new HTTP.Request("get", m_BaseUrl + "/game", header);

            req.Send((request) =>
            {
                Debug.Log(request.response.status);

                bool result = false;
                Hashtable obj = (Hashtable)JSON.JsonDecode(request.response.Text, ref result);

                callback.Invoke(obj);
            });
        }

        public void CreateGame(int uid, int fid, Action<Hashtable> callback)
        {
            if (callback == null)
            {
                return;
            }

            Dictionary<string, string> header = new Dictionary<string, string>();
            header.Add("uid", uid.ToString());
            header.Add("fid", fid.ToString());

            HTTP.Request req = new HTTP.Request("post", m_BaseUrl + "/game/create", header);

            req.Send((request) =>
            {
                Debug.Log(request.response.status);

                bool result = false;
                Hashtable obj = (Hashtable)JSON.JsonDecode(request.response.Text, ref result);

                callback.Invoke(obj);
            });
        }

        public void UpdateGame(int uid, int gid, ArrayList answers, Action<Hashtable> callback)
        {
            if (callback == null)
            {
                return;
            }

            Dictionary<string, string> header = new Dictionary<string, string>();
            header.Add("uid", uid.ToString());
            header.Add("gid", gid.ToString());
            header.Add("answers", JSON.JsonEncode((object)answers));

            HTTP.Request req = new HTTP.Request("post", m_BaseUrl + "/game", header);

            req.Send((request) =>
            {
                Debug.Log(request.response.status);

                bool result = false;
                Hashtable obj = (Hashtable)JSON.JsonDecode(request.response.Text, ref result);

                callback.Invoke(obj);
            });
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using SimpleStateMachine;
using Communication;

public class GameManager : GameEntity 
{
    private static GameManager instance;

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
            }

            return instance;
        }
    }

    public string m_ServiceURL;
    private ScreenManager m_ScreenManager;

    [HideInInspector]
    public PlayerController m_PlayerController;

    public ScreenManager ScreenManager
    {
        get
        {
            return m_ScreenManager;
        }
    }

    protected override void Start()
    {
        base.Start();
        DontDestroyOnLoad(gameObject);

        Communicator.Instance.BaseURL = m_ServiceURL;
        m_PlayerController = new PlayerController();

        m_ScreenManager = GetComponentInChildren<ScreenManager>();

        StartCoroutine(StartGame());
    }

    private IEnumerator StartGame()
    {
        yield return null;
        SetState(new StateMenu());
    }

    public void UpdatePlayerData()
    {
        Communicator.Instance.LoginUser(m_PlayerController.m_Name, (response) =>
        {
            if (response != null)
            {
                GameManager.Instance.m_PlayerController.ParseData(response);
            }
        });
    }
}

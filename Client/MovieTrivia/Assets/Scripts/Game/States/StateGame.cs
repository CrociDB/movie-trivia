﻿using UnityEngine;
using System.Collections;
using SimpleStateMachine;
using Communication;

public class StateGame : State 
{
    GameManager m_GameManager;

    TriviaController m_Trivia;
    TriviaController.MovieDetail m_CurrentMovie;
    ScreenGame m_Screen;

    int m_Id;

    public StateGame(int gid) : base()
    {
        m_Id = gid;
    }

    public override void Enter()
    {
        m_GameManager = (GameManager)m_Entity;
        m_GameManager.ScreenManager.ActivateScreen(ScreenManager.ScreenType.Loading);

        m_Screen = (ScreenGame)m_GameManager.ScreenManager.GetScreen(ScreenManager.ScreenType.Game);
        m_Screen.m_ActionClickedAlternative = SelectAlternative;

        Communicator.Instance.GetGame(m_GameManager.m_PlayerController.m_Id, m_Id, (response) =>
        {
            m_Trivia = new TriviaController(response);
            StartGame();
        });
    }

    private void SelectAlternative(int index)
    {
        bool right = m_Trivia.UpdateGame(index);

        if (right)
        {
            m_Screen.ShowRightFeedback();
        }
        else
        {
            m_Screen.ShowWrongFeedback();
        }

        m_GameManager.StartCoroutine(DelayedStartGame());
    }

    private IEnumerator DelayedStartGame()
    {
        yield return new WaitForSeconds(.5f);
        StartGame();
    }

    private void StartGame()
    {
        m_CurrentMovie = m_Trivia.GetQuestion();
        if (m_CurrentMovie != null)
        {
            m_Screen.Populate(m_Trivia);
            m_GameManager.ScreenManager.ActivateScreen(ScreenManager.ScreenType.Game);
        }
        else
        {
            FinishGame();
        }
    }

    private void FinishGame()
    {
        // Convert data to JSON kinda object
        ArrayList answers = new ArrayList();
        foreach (var answer in m_Trivia.m_Answers)
        {
            Hashtable ha = new Hashtable();

            ha.Add("id", answer.m_MovieId);
            ha.Add("year", answer.m_YearAnswered);

            answers.Add((object)ha);
        }
        
        // Send things to server
        Communicator.Instance.UpdateGame(m_GameManager.m_PlayerController.m_Id, m_Trivia.m_Id, answers, (response) =>
        {
            int status = (int)response["status"];
            if (status == 2)
            {
                m_GameManager.UpdatePlayerData();
                m_Screen.ShowResultsFinal(response);
            }
            else
            {
                m_Screen.ShowCurrentResult(response);
            }
            
        });

    }
}

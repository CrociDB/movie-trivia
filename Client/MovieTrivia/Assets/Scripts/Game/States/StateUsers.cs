﻿using UnityEngine;
using System.Collections;
using SimpleStateMachine;
using Communication;

public class StateUsers : State 
{
    GameManager m_GameManager;

    ScreenUsers m_Screen;

    public override void Enter()
    {
        m_GameManager = (GameManager)m_Entity;
        m_GameManager.ScreenManager.ActivateScreen(ScreenManager.ScreenType.Loading);

        Communicator.Instance.GetUserList(GameManager.Instance.m_PlayerController.m_Id, (response) =>
        {
            m_GameManager.ScreenManager.ActivateScreen(ScreenManager.ScreenType.Users);

            m_Screen = (ScreenUsers)m_GameManager.ScreenManager.CurrentScreen;
            m_Screen.m_ActionCreateGame = CreateGame;
            m_Screen.PopulateWithData(response);
        });
    }

    private void CreateGame(int fid)
    {
        m_GameManager.ScreenManager.ActivateScreen(ScreenManager.ScreenType.Loading);
        Communicator.Instance.CreateGame(m_GameManager.m_PlayerController.m_Id, fid, (response) =>
        {
            int gid = (int)response["id"];
            m_GameManager.SetState(new StateGame(gid));
        });
    }

    public override void Exit()
    {
        base.Exit();
        m_Screen.m_ActionCreateGame = null;
    }
}

﻿using UnityEngine;
using System.Collections;
using SimpleStateMachine;

public class StateMenu : State 
{
    GameManager m_GameManager;

    public override void Enter()
    {
        m_GameManager = (GameManager)m_Entity;
        m_GameManager.ScreenManager.ActivateScreen(ScreenManager.ScreenType.Menu);
    }
}

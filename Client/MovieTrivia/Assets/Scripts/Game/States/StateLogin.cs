﻿using UnityEngine;
using System.Collections;
using SimpleStateMachine;
using Communication;

public class StateLogin : State 
{
    GameManager m_GameManager;
    ScreenLogin m_Screen;

    public override void Enter()
    {
        m_GameManager = (GameManager)m_Entity;
        m_GameManager.ScreenManager.ActivateScreen(ScreenManager.ScreenType.Login);

        m_Screen = (ScreenLogin)m_GameManager.ScreenManager.CurrentScreen;
        m_Screen.m_ActionLogin = Login;
    }

    private void Login(string name)
    {
        m_GameManager.ScreenManager.ActivateScreen(ScreenManager.ScreenType.Loading);

        Communicator.Instance.LoginUser(name, (response) =>
        {
            if (response != null)
            {
                GameManager.Instance.m_PlayerController.ParseData(response);
                GameManager.Instance.SetState(new StateGameList());
            }
        });
    }

    public override void Exit()
    {
        m_Screen.m_ActionLogin = null;
    }
}

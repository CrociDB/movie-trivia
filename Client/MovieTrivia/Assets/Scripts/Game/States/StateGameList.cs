﻿using UnityEngine;
using System.Collections;
using SimpleStateMachine;
using Communication;

public class StateGameList : State 
{
    GameManager m_GameManager;

    ScreenGameList m_Screen;

    public override void Enter()
    {
        m_GameManager = (GameManager)m_Entity;
        m_GameManager.ScreenManager.ActivateScreen(ScreenManager.ScreenType.Loading);

        Communicator.Instance.GetGameList(GameManager.Instance.m_PlayerController.m_Id, (response) =>
        {
            m_GameManager.ScreenManager.ActivateScreen(ScreenManager.ScreenType.GameList);

            m_Screen = (ScreenGameList)m_GameManager.ScreenManager.CurrentScreen;
            m_Screen.m_ActionPlayGame = PlayGame;
            m_Screen.PopulateWihData(response);
        });
    }

    private void PlayGame(int gid)
    {
        m_GameManager.SetState(new StateGame(gid));
    }

    public override void Exit()
    {
        base.Exit();
        m_Screen.m_ActionPlayGame = null;
    }
}

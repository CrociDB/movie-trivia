﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScreenManager : MonoBehaviour 
{
    public enum ScreenType
    {
        Menu,
        Login,
        GameList,
        Users,
        Game,
        Loading
    };

    public List<Screen> m_Screens;
    private Screen m_CurrentScreen;

    public Screen CurrentScreen
    {
        get
        {
            return m_CurrentScreen;
        }
    }

    void Start()
    {
        DeactivateAllScreens();
    }

    private void DeactivateAllScreens()
    {
        foreach (Screen screen in m_Screens)
        {
            screen.Deactivate();
        }
    }

    public void ActivateScreen(ScreenType type)
    {
        if (m_CurrentScreen != null)
        {
            m_CurrentScreen.Deactivate();
        }

        foreach (Screen screen in m_Screens)
        {
            if (screen.m_ScreenType == type)
            {
                m_CurrentScreen = screen;
                m_CurrentScreen.Activate();
                break;
            }
        }
    }

    public Screen GetScreen(ScreenType type)
    {
        foreach (Screen screen in m_Screens)
        {
            if (screen.m_ScreenType == type)
            {
                return screen;
            }
        }

        return null;
    }
}

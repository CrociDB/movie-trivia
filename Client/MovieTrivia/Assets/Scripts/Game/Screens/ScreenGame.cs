﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ScreenGame : Screen 
{
    public Text[] m_Alternatives;
    public Text m_Score;
    public Text m_Progress;
    public Text m_MovieName;
    public RawImage m_MovieCover;

    public RectTransform m_RightPanel;
    public RectTransform m_WrongPanel;

    public RectTransform m_ResultPanel;
    public Text m_ResultTitle;
    public Text m_ResultUsers;
    public Text m_ResultScore;

    public Action<int> m_ActionClickedAlternative;

    private Coroutine m_LoadingRoutine;

    public ScreenGame() : base()
    {
        m_ScreenType = ScreenManager.ScreenType.Game;
    }

    public void Populate(TriviaController triviaController)
    {
        HidePanels();

        if (m_LoadingRoutine != null)
        {
            GameManager.Instance.StopCoroutine(m_LoadingRoutine);
            m_MovieCover.texture = null;
        }

        m_MovieName.text = triviaController.GetQuestion().m_Name;

        for (int i = 0; i < triviaController.GetQuestion().m_YearAlternatives.Count; i++)
        {
            m_Alternatives[i].text = triviaController.GetQuestion().m_YearAlternatives[i].ToString();
        }

        m_Score.text = "Score: " + triviaController.m_CurrentUserScore;
        m_Progress.text = "Question: " + (triviaController.m_CurrentMovie + 1) + " / "  + triviaController.m_Movies.Count;

        m_LoadingRoutine = GameManager.Instance.StartCoroutine(LoadCover(triviaController.GetQuestion().m_Cover));
    }

    private IEnumerator LoadCover(string url)
    {
        var wwwTexture = new WWW(url);
        
        while (!wwwTexture.isDone)
        {
            yield return null;
        }

        m_MovieCover.texture = wwwTexture.texture;

        HidePanels();
    }

    public void ClickAlternative(int index)
    {
        if (m_ActionClickedAlternative != null)
        {
            m_ActionClickedAlternative.Invoke(index);
        }
    }

    public void ShowRightFeedback()
    {
        m_RightPanel.gameObject.SetActive(true);
    }

    public void ShowWrongFeedback()
    {
        m_WrongPanel.gameObject.SetActive(true);
    }

    public void HidePanels()
    {
        m_RightPanel.gameObject.SetActive(false);
        m_WrongPanel.gameObject.SetActive(false);
        m_ResultPanel.gameObject.SetActive(false);
    }

    public void ShowResultsFinal(Hashtable response)
    {
        int myId = GameManager.Instance.m_PlayerController.m_Id;
        int invite = (int)response["user_invite"];
        int accepted = (int)response["user_accepted"];
        int inviteScore = (int)response["score_user_invited"];
        int acceptedScore = (int)response["score_user_accepted"];

        if (inviteScore == acceptedScore)
        {
            m_ResultTitle.text = "Match Draw!";
        }
        else if (inviteScore > acceptedScore)
        {
            if (invite == myId)
            {
                m_ResultTitle.text = "You Won!";
            }
            else
            {
                m_ResultTitle.text = "You Lost!";
            }
        }
        else
        {
            if (invite == myId)
            {
                m_ResultTitle.text = "You Lost!";
            }
            else
            {
                m_ResultTitle.text = "You Won!";
            }
        }

        m_ResultUsers.text = "Score";
        m_ResultScore.text = inviteScore + " - " + acceptedScore;

        m_ResultPanel.gameObject.SetActive(true);
    }

    public void ShowCurrentResult(Hashtable response)
    {
        int myId = GameManager.Instance.m_PlayerController.m_Id;
        int invite = (int)response["user_invite"];
        int accepted = (int)response["user_accepted"];
        int inviteScore = (int)response["score_user_invited"];
        int acceptedScore = (int)response["score_user_accepted"];
        string invitedUser = (string)response["invited_user"];
        string acceptedUser = (string)response["accepted_user"];

        m_ResultTitle.text = "Waiting...";

        m_ResultUsers.text = "Your score";
        m_ResultScore.text = inviteScore.ToString();

        m_ResultPanel.gameObject.SetActive(true);
    }

    public void GoBack()
    {
        GameManager.Instance.SetState(new StateGameList());
    }
}

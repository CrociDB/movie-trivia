﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class GameInfo : MonoBehaviour 
{
    public struct GameData
    {
        public int id;
        public int userInvited;
        public int userAccepted;
        public int scoreInvited;
        public int scoreAccepted;
        public string userNameInvited;
        public string userNameAccepted;
        public int status;
    };

    [HideInInspector]
    public GameData m_GameData;

    public Action<int> m_ActionPlayGame;

    public Text m_TextGame;
    public Text m_TextStatus;

    public void Click()
    {
        if (m_ActionPlayGame != null)
        {
            m_ActionPlayGame.Invoke(m_GameData.id);
        }
    }

    public void Populate(Hashtable data)
    {
        m_GameData = new GameData();

        m_GameData.id = (int)data["id"];
        m_GameData.userInvited = (int)data["user_invite"];
        m_GameData.userAccepted = (int)data["user_accepted"];
        m_GameData.scoreInvited = (int)data["score_user_invited"];
        m_GameData.scoreAccepted = (int)data["score_user_accepted"];
        m_GameData.userNameInvited = (string)data["invited_user"];
        m_GameData.userNameAccepted = (string)data["accepted_user"];
        m_GameData.status = (int)data["status"];

        bool myTurn = (m_GameData.status == 0 && m_GameData.userInvited == GameManager.Instance.m_PlayerController.m_Id) ||
                        (m_GameData.status == 1 && m_GameData.userAccepted == GameManager.Instance.m_PlayerController.m_Id);

        if (m_TextGame != null)
        {
            string text = m_GameData.userNameInvited + " - " + m_GameData.userNameAccepted;
            text += "\n" + m_GameData.scoreInvited + " - " + m_GameData.scoreAccepted;

            m_TextGame.text = text;
        }

        if (m_TextStatus != null)
        {
            m_TextStatus.text = myTurn ? "Your turn!" : "Waiting...";
        }

        GetComponent<Button>().enabled = myTurn;
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class UserButton : MonoBehaviour 
{
    [HideInInspector]
    public int m_UserId;
    [HideInInspector]
    public string m_UserName;

    public Text m_TextName;

    public Action<int> m_ActionCreateGame;

    public void Click()
    {
        if (m_ActionCreateGame != null)
        {
            m_ActionCreateGame.Invoke(m_UserId);
        }
    }

    public void Populate(int id, string name)
    {
        m_UserId = id;
        m_UserName = name;

        if (m_TextName != null)
        {
            m_TextName.text = m_UserName;
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.Assertions;

public class ScreenLogin : Screen 
{
    public Action<string> m_ActionLogin;

    public InputField m_NameInput;

    public ScreenLogin() : base()
    {
        m_ScreenType = ScreenManager.ScreenType.Login;
    }

    public void Play()
    {
        Assert.IsNotNull(m_NameInput);

        if (m_ActionLogin != null)
        {
            m_ActionLogin.Invoke(m_NameInput.text);
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class Screen : MonoBehaviour 
{
    public ScreenManager.ScreenType m_ScreenType;

    public void Activate()
    {
        Populate();
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }

    protected virtual void Populate()
    {

    }
}

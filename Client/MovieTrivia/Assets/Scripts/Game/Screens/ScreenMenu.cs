﻿using UnityEngine;
using System.Collections;

public class ScreenMenu : Screen 
{
    public ScreenMenu() : base()
    {
        m_ScreenType = ScreenManager.ScreenType.Menu;
    }

    public void Play()
    {
        GameManager.Instance.SetState(new StateLogin());
    }

    public void Credits()
    {

    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ScreenUsers : Screen 
{
    private List<UserButton> m_Users;

    public Transform m_Content;
    public GameObject m_ButtonPrefab;

    public Action<int> m_ActionCreateGame;

    public ScreenUsers() : base()
    {
        m_ScreenType = ScreenManager.ScreenType.Users;

        m_Users = new List<UserButton>();
    }

    public void Back()
    {
        GameManager.Instance.SetState(new StateGameList());
    }

    internal void PopulateWithData(ArrayList response)
    {
        foreach (var user in m_Users)
        {
            Destroy(user.gameObject);
        }

        m_Users.Clear();

        foreach (var obj in response)
        {
            Hashtable player = (Hashtable)obj;

            UserButton button = ((GameObject)GameObject.Instantiate(m_ButtonPrefab)).GetComponent<UserButton>();
            button.Populate((int)player["id"], (string)player["name"]);
            button.m_ActionCreateGame = m_ActionCreateGame;
            button.transform.parent = m_Content;

            m_Users.Add(button);
        }
    }
}

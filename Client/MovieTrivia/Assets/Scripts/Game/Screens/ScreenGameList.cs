﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class ScreenGameList : Screen 
{
    private List<GameInfo> m_Games;

    public Text m_Name;
    public Text m_Score;
    public Text m_Total;

    public Transform m_Content;
    public GameObject m_GameInfoPrefab;

    public Action<int> m_ActionPlayGame;

    public ScreenGameList() : base()
    {
        m_ScreenType = ScreenManager.ScreenType.GameList;

        m_Games = new List<GameInfo>();
    }

    protected override void Populate()
    {
        base.Populate();

        if (m_Name != null)
        {
            m_Name.text = GameManager.Instance.m_PlayerController.m_Name;
        }

        if (m_Score != null)
        {
            m_Score.text = "Score: " + GameManager.Instance.m_PlayerController.m_Won.ToString();
        }

        if (m_Total != null)
        {
            m_Total.text = "Total games played: " + GameManager.Instance.m_PlayerController.m_Played.ToString();
        }
    }

    public void NewGame()
    {
        GameManager.Instance.SetState(new StateUsers());
    }

    public void PopulateWihData(ArrayList response)
    {
        foreach (var game in m_Games)
        {
            Destroy(game.gameObject);
        }

        m_Games.Clear();

        foreach (var obj in response)
        {
            Hashtable game = (Hashtable)obj;

            GameInfo button = ((GameObject)GameObject.Instantiate(m_GameInfoPrefab)).GetComponent<GameInfo>();
            button.Populate(game);
            button.m_ActionPlayGame = m_ActionPlayGame;
            button.transform.parent = m_Content;

            m_Games.Add(button);
        }
    }
}

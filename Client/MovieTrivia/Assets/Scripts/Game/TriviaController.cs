﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class TriviaController
{
    public class MovieDetail
    {
        public int m_Id;
        public string m_Name;
        public int m_Year;
        public string m_Cover;
        public string m_Link;

        public List<int> m_YearAlternatives;

        public MovieDetail(Hashtable rawData)
        {
            m_Id = (int)rawData["id"];
            m_Name = (string)rawData["name"];
            m_Year = (int)rawData["year"];
            m_Cover = (string)rawData["cover_link"];
            m_Link = (string)rawData["link"];

            CreateYears();
            ShuffleYears();
        }

        private void CreateYears()
        {
            m_YearAlternatives = new List<int>();
            m_YearAlternatives.Add(m_Year);
            m_YearAlternatives.Add(m_Year + GetRandomYearAdd(m_Year, m_YearAlternatives.ToArray()));
            m_YearAlternatives.Add(m_Year + GetRandomYearAdd(m_Year, m_YearAlternatives.ToArray()));
        }

        private int GetRandomYearAdd(int year, int[] noList)
        {
            int currentYear = System.DateTime.Now.Year;

            int random = 0;
            do
            {
                random = Random.Range(-16, 16);
            } while ((random + year) > currentYear || noList.Contains(random + year));

            return random;
        }

        private void ShuffleYears()
        {
            var r = new System.Random();
            m_YearAlternatives.Sort((x, y) => r.Next(-1, 1));
        }
    };

    public struct MovieAnswers
    {
        public int m_MovieId;
        public int m_YearAnswered;
    };


    public int m_Id;
    public int m_UserInvite;
    public int m_UserAccepted;
    public int m_Status;
    public string[] m_SortedMovies;

    public int m_CurrentUserScore;

    public List<MovieDetail> m_Movies;
    public List<MovieAnswers> m_Answers;

    public int m_CurrentMovie;

    public TriviaController(Hashtable rawData)
    {
        m_CurrentUserScore = 0;

        m_Id = (int)rawData["id"];
        m_UserInvite = (int)rawData["user_invite"];
        m_UserAccepted = (int)rawData["user_accepted"];
        m_Status = (int)rawData["status"];

        m_SortedMovies = ((string)rawData["movies"]).Split(',');

        ArrayList movies = (ArrayList)rawData["movies_detail"];

        m_Movies = new List<MovieDetail>();
        m_Answers = new List<MovieAnswers>();

        foreach (var rawMovie in movies)
        {
            m_Movies.Add(new MovieDetail((Hashtable)rawMovie));
        }

        ShuffleMovies();
    }

    private void ShuffleMovies()
    {
        List<MovieDetail> newList = new List<MovieDetail>();

        foreach (string id in m_SortedMovies)
        {
            newList.Add(m_Movies.Find((movie) => movie.m_Id == int.Parse(id)));
        }

        m_Movies = newList;
    }

    public MovieDetail GetQuestion()
    {
        if (m_CurrentMovie >= m_Movies.Count)
        {
            return null;
        }

        return m_Movies[m_CurrentMovie];
    }

    public bool UpdateGame(int option)
    {
        bool right = false;

        var movie = GetQuestion();
        if (movie != null)
        {
            m_Answers.Add(new MovieAnswers() { m_MovieId = movie.m_Id, m_YearAnswered = movie.m_YearAlternatives[option] });

            if (movie.m_YearAlternatives[option] == movie.m_Year)
            {
                m_CurrentUserScore += 5;
                right = true;
            }
            else
            {
                m_CurrentUserScore = Mathf.Clamp(m_CurrentUserScore - 3, 0, 100);
            }

            m_CurrentMovie++;
        }

        return right;
    }
}

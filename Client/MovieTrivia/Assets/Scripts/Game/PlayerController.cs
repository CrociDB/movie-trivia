﻿using UnityEngine;
using System.Collections;

public class PlayerController
{
    public int m_Id;
    public string m_Name;
    public int m_Won;
    public int m_Played;

    public void ParseData(Hashtable data)
    {
        m_Id = (int)data["id"];
        m_Name = (string)data["name"];
        m_Played = (int)data["played"];
        m_Won = (int)data["won"];
    }
}
